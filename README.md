# Social.Coop Tech Group: Operations repository

This repository is used for tracking technical [issues](https://git.coop/social.coop/tech/operations/-/issues) related to SocialCoop services and the Tech Working Group's activities. More documentation is available in the [Technical Working Group](https://git.coop/social.coop/general/-/wikis/tech-working-group/Tech-Working-Group) section of the SocialCoop Wiki.

Prior to the wiki pages being available in the general wiki they were created in this repository using GitLab's wiki. An archive of the Git repository used for these wiki pages is available here as `wiki-archive.bundle`. Here are the steps to getting back the historical wiki content:

```bash
$ wget https://git.coop/social.coop/tech/operations/-/raw/master/wiki-archive.bundle
$ mkdir wiki-archive
$ cd wiki-archive
$ git init
$ git pull ../wiki-archive.bundle
```
